import React from 'react'
import { useAsync } from 'react-async-hook'
import AwesomeDebouncePromise from 'awesome-debounce-promise'
import useConstant from 'use-constant'

const useDebouncedSave = saveFunction => {
  const [inputText, setInputText] = React.useState('')

  const debouncedSaveFunction = useConstant(() =>
    AwesomeDebouncePromise(saveFunction, 300)
  )

  useAsync(async () => {
    debouncedSaveFunction(inputText)
    // if (inputText.length === 0) {
    //   return []
    // } else {
    //   return debouncedSaveFunction(inputText)
    // }
  }, [debouncedSaveFunction, inputText])

  return {
    inputText,
    setInputText
  }
}

export default useDebouncedSave

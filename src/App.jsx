import React from 'react'
import Icon from 'kit/Icon'
import Input from 'kit/Input'
import Button from 'kit/Button'
import PopoverArea from 'kit/PopoverArea'
import '@/styles/index.scss'
import Topology from './Topology/Topology'

const { location } = window
const defaultHost = location.host

if (location.hostname !== 'localhost' && location.protocol === 'http:')
  location.href = location.href.replace('http:', 'https:')

const initialButtons = [
  {
    name: 'Kubernetes A',
    prefix: 'k8s',
    host: defaultHost ?? '',
    suffix: 'kube-service-1',
    icon: 'kubernetes',
    type: 'request'
  },
  {
    name: 'Kubernetes B',
    prefix: 'k8s',
    host: defaultHost ?? '',
    suffix: 'kube-service-2',
    icon: 'kubernetes',
    type: 'request'
  },
  {
    name: 'Lambda A',
    prefix: 'lambda',
    host: defaultHost ?? '',
    suffix: 'lambda-service-1',
    icon: 'lambda',
    type: 'request'
  },
  {
    name: 'Lambda B',
    prefix: 'lambda',
    host: defaultHost ?? '',
    suffix: 'lambda-service-2',
    icon: 'lambda',
    type: 'request'
  },
  {
    name: 'Jenkins',
    prefix: 'jenkins',
    host: defaultHost ?? '',
    icon: 'jenkins',
    type: 'link'
  }
]

const App = () => {
  const [buttons, updateButtons] = React.useState(initialButtons)
  const [responseText, setResponseText] = React.useState('')
  const [editButton, setEditButton] = React.useState(null)

  const requestData = url => {
    fetch(url)
      .then(r => r.json())
      .then(r => setResponseText(JSON.stringify(r)))
  }

  return (
    <div className="max-w-[90%] sm:max-w-[85%] mx-auto flex flex-col lg:flex-row justify-between">
      <div className="w-full lg:w-[50%] mb-5">
        <img className="w-[150px] sm:w-[200px] mt-5" src="/logo.svg" alt="Logo" />
        <h1 className="text-2xl sm:text-mm font-semibold mt-10 mb-4">
          Welcome to Simloud World
        </h1>

        <h3 className="text-sm sm:text-base font-medium mb-3">
          Choose the technology that interests you:
        </h3>

        <div className="flex flex-wrap justify-between w-full md:w-[95%]">
          {buttons.map(
            ({ name, host, prefix, suffix, icon, type }, i) =>
              (editButton === null || editButton === i) && (
                <div className="flex w-full sm:w-[45%]" key={name}>
                  <div className="flex justify-between space-x-3 w-full">
                    <div className="w-[90%]">
                      {type === 'request' && (
                        <Button
                          type="primary"
                          className="w-full font-normal mb-5 h-[47px] flex items-center justify-center rounded-sm"
                          onClick={() =>
                            requestData(`https://${prefix}.${host}/${suffix}/`)
                          }
                        >
                          <img
                            src={`/icons/${icon}.svg`}
                            alt="logo"
                            height="35"
                            className="mr-2"
                          />{' '}
                          {name}
                        </Button>
                      )}
                      {type === 'link' && (
                        <a
                          href={`https://${prefix}.${host}/`}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          <Button
                            type="primary"
                            className="w-full flex items-center justify-center mb-5 h-[47px]"
                          >
                            <img
                              src={`/icons/${icon}.svg`}
                              alt="logo"
                              height="35"
                              className="mr-2"
                            />{' '}
                            {name}
                          </Button>
                        </a>
                      )}
                    </div>
                    {editButton !== i && (
                      <div>
                        <PopoverArea
                          popover={{
                            interactive: false,
                            content: 'Edit mode'
                          }}
                        >
                          <Icon
                            name="HiOutlineCog"
                            className="mt-4"
                            onClick={() => setEditButton(i)}
                          />
                        </PopoverArea>
                      </div>
                    )}
                  </div>
                </div>
              )
          )}
        </div>

        {editButton !== null && (
          <>
            <div className="w-full flex">
              <Input
                className="border !border-gray-300 outline-none focus:outline-none  focus:border-blue-500 px-3 py-1.5"
                value={buttons[editButton].prefix}
                onChange={({ target: { value } }) =>
                  updateButtons(
                    buttons.map((button, i) =>
                      i === editButton ? { ...button, prefix: value } : button
                    )
                  )
                }
              />
              <Input
                className="w-[18px] border-none text-center"
                value="."
                disabled={true}
              />
              <Input
                className="border !border-gray-300 outline-none focus:outline-none  focus:border-blue-500 px-3 py-1.5"
                value={buttons[editButton].host}
                onChange={({ target: { value } }) =>
                  updateButtons(
                    buttons.map((button, i) =>
                      i === editButton ? { ...button, host: value } : button
                    )
                  )
                }
              />
              {buttons[editButton].type === 'request' && (
                <>
                  <Input
                    className="w-[18px] border-none text-center"
                    value="/"
                    disabled={true}
                    style={{ paddingLeft: 3, paddingRight: 3 }}
                  />
                  <Input
                    className="border !border-gray-300 outline-none focus:outline-none  focus:border-blue-500 px-3 py-1.5"
                    value={buttons[editButton].suffix}
                    onChange={({ target: { value } }) =>
                      updateButtons(
                        buttons.map((button, i) =>
                          i === editButton ? { ...button, suffix: value } : button
                        )
                      )
                    }
                  />
                </>
              )}
            </div>

            <div className="text-right">
              <Button
                className="mt-2 px-4 py-1.5 rounded-sm font-normal"
                type="primary"
                onClick={() => setEditButton(null)}
              >
                Save
              </Button>
            </div>
          </>
        )}
        <textarea
          placeholder="Response goes here..."
          rows="7"
          className="mt-5 w-full border border-gray-300 p-3 placeholder-gray-400 focus:outline-none  focus:border-blue-500"
          value={responseText}
          onChange={() => {}}
        />
      </div>
      <div className="w-full lg:w-[45%]">
        <Topology {...{ q: ['all'], animPath: ['ec2', 'lambda'] }} />
      </div>
    </div>
  )
}

export default App

import React from 'react'
import SVG from 'react-inlinesvg'
import * as HeroIcons from 'react-icons/hi'
import PopoverWrapper from 'kit/PopoverWrapper'

const IconCmp = React.forwardRef(
  ({ name, wrapperClassName = '', className = '', onClick = null }, ref) => {
    const iconClassName = `icon-default ${
      name === 'Spin' ? 'animate-spin' : ''
    } ${className}`

    return (
      <span className={wrapperClassName} ref={ref} onClick={onClick}>
        {HeroIcons[name] ? (
          HeroIcons[name]({ className: iconClassName })
        ) : (
          <SVG src={`/custom-icons/${name}.svg`} className={iconClassName} />
        )}
      </span>
    )
  }
)

const Icon = props => <PopoverWrapper {...props} {...{ cmp: <IconCmp /> }} />

export default Icon

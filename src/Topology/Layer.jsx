import React from 'react'

const Layer = ({ img }) => (
  <div
    className="absolute h-full w-full bg-transparent bg-center bg-no-repeat bg-contain"
    style={{
      backgroundImage: `url(/topology/${img}.svg)`
    }}
  />
)

export default Layer

import React from 'react'
import Layer from './Layer'
import EC2 from './EC2'

const ALL_ITEMS = ['lambda', 'dynamodb', 'ecr', 'sqs', 'rds_multiAZ', 's3']

let ec2 = {
  type: 'm4',
  size: 'xlarge'
}

const Topology = ({
  ec2type = '',
  ec2size = '',
  q = [],
  animPath = [],
  debug = false
}) => {
  if (q.includes('all')) q = ALL_ITEMS
  if (ec2type) ec2.type = ec2type
  if (ec2size) ec2.size = ec2size

  return (
    <div className="topology-page">
      <div className="block h-screen relative">
        <Layer img="canvas" />
        <Layer img="watermark" />

        <Layer img="route-53-items" />
        <Layer img="alb-items" />
        {q.includes('lambda') && <Layer img="alb-lambda-items" />}
        <Layer img="igw-items" />
        <Layer img="igw-natgw-items" />
        {q.includes('lambda') && <Layer img="lambda-items" />}
        <Layer img="ec2-items" />
        {q.includes('lambda') && <Layer img="ec2-lambda-items" />}
        <Layer img="natgw-ec2-items" />

        {animPath.includes('lambda') && <Layer img="lambda-animation" />}
        {animPath.includes('ec2') && <Layer img="ec2-animation" />}

        <Layer img="alb" />
        <Layer img="cloud-trail" />
        <Layer img="cw-alarm" />
        <Layer img="cw-mnt" />
        {q.includes('dynamodb') && <Layer img="dyn-db" />}
        {q.includes('ecr') && <Layer img="ecr" />}
        <Layer img="eks" />
        <Layer img="iam" />
        <Layer img="igw" />
        <Layer img="jenkins" />
        {q.includes('sqs') && <Layer img="sqs" />}
        <Layer img="natgw" />
        {q.includes('lambda') && <Layer img="lambda" />}
        {q.includes('rds_multiAZ') && <Layer img="rds-multiple" />}
        {q.includes('rds_single') && <Layer img="rds-single" />}
        <Layer img="route-53" />
        {q.includes('s3') && <Layer img="s3" />}
        <Layer img="secret-manager" />
        <Layer img="user" />
        <EC2 type={ec2.type} size={ec2.size} />

        {debug && <Layer img="debug" />}
      </div>
    </div>
  )
}

export default Topology

## fe-caneveral

This repository is used for deploying microservice fe-canaveral. It is possible to use files from this directory to deploy frontend service.

## Regex option

If you need to change the value of regex parameter, it's necessary to edit Simloudfile.yaml.
There are 2 possible options for regex value:
- true - for service, the regex rules will be applied based on the already set and specifically configured regex rules in Simloudfile.yaml. It could be a custom value.
- false - service will be deployed without any regex rules and according to already specified configuration in URL path.

Example of code block:

```
regex:
    enabled: false
    rewrite-target: /$2$3$4
````
```
regex:
    enabled: true
    rewrite-target: /$2$3$4
```


## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead (updating master) it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

## Local development

You can use `docker-compose` to launch application locally without installing any packages to the host machine.
Commands:

- `docker-compose build`: launch once to generate container (no need to launch it for each change in application dependencies)
- `docker-compose up`: install application dependencies and launch app in Open [https://local.simloud.be:443](https://local.simloud.be:443) (analogue of `yarn && yarn start`)
- `docker-compose run --rm app <command>`: launch custom command inside container, for example
  - `docker-compose run --rm app sh`: exec into container in interactive mode
  - `docker-container run --rm app yarn test`: launch test runner

First time, you have to add next line to the `/etc/hosts`:

```text
127.0.0.1 local.simloud.be
```

Local application uses Simloud Cognito from DEV env. If you're not logged in on DEV, or your Cognito token was expired, you will be redirected to the login page.
One you provide your credentials, you will be redirected to the DEV env automatically, so you have to re-open local page again.

If you want to point local FE application to the one or more local BE services (instead of their remote public equivalents from DEV env), you should edit `src/endpoints/dev.endpoints.js` (or another `*.endpoints.js` file if you use non-`dev` branch, see `src/endpoints/index.js`) and replace remote hosts by local hosts.
For example, to use local `infrastructure-service`, you should replace `https://k8s.portal.simloud.be/infrastructure-service/api/v1/` by `http://172.17.0.1:8010/api/v1/`.
Also, if you want to connect local FE application to the remote BE services not via public hosts but using port forward (see [RnD](https://bitbucket.org/simloud/rnd/src/master/port_forward_for_local_development/)), you can use the same approach.
For example, to use remote `infrastructure-service` via port forward, you should replace `https://k8s.portal.simloud.be/infrastructure-service/api/v1/` by `http://172.17.0.1:6010/api/v1/`.
Once you change hosts, you don't have to re-launch `docker-compose up` - FE web-service is automatically reloaded on each change in code.

You may have to configure browser in case if you use `http://` for some BE services, because it may be blocked by default due to `mixed-content`.
In Chromium-based browsers (Google Chrome, Opera, Vivaldi etc.) you should click on icon of red lock left from `https://local.simloud.be` in address bar, select "Site settings", allow "Insecure content".


General structure of fe-canaveral service was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Additional documentation is placed by links:
- [**"How to use Simloud files"**](https://docs.simloud.com/en/how-to-use-simloud-files/)

- [**"How to create and manage your SSH keys"**](https://docs.simloud.com/en/getting-started/#managing-the-ssh-keys)

- [**"How to work with repositories"**](https://docs.simloud.com/en/getting-started/#add-new-git-repositories-services)

